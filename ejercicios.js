//Ejercicio número 1 (punto 2 del examen)

function piramide() {
  window.open("piramide.html");
}

//---------------------------------------------------------------------------------------------------

//Ejercicio número 2 (punto 3 del examen)

let valor = 200;
//comprueba si el número es correcto y si no, calcula uno aleatorio.
function validar() {
  let x = prompt("Introduce un número entero menor que 100.");
  /**
   * el primer if:
   *  x != null --> es una condición por si se pulsa cancelar.
   *  x != "" --> es una condición para el caso de pulsar aceptar sin meter ningún valor.
   *  el resto de las condiciones limitan el valor introducido entre 0 y 100, sin incluir el 100.
   */
  if (x != null && x != "" && x >= 0 && x < 100) {
    alert("Tu número es correcto");
    valor = x;
  } else if (x == null) {
    // En el caso de darle al botón cancelar, saldrá el mensaje y volveremos a llamar a nuestra función validar
    alert("Hasta luego!");
  } else {
    // En el caso de que el valor introducido no cumpla las condiciones, calcula un número aleatorio y utiliza ese.
    valor = Math.floor(Math.random() * 100);
    alert("El número no es correcto. Usaré el que yo quiera: " + valor);
  }
}

//-------------------------------------------------------------------------------------------------------

//Ejercicio número 3 (punto 4 del examen)

//Se calcula la fecha actual y se calcula la nueva sumando los días que corresponden al número del ejercicio anterior.
function calcularFecha() {
  let fechaActual = new Date(Date.now());
  fechaActual.setDate(fechaActual.getDate() + parseInt(valor));
  alert(fechaBonita(fechaActual));
}
//Trnasforma los milisegundos a formato día, mes, año.
function fechaBonita(fecha) {
  return (
    fecha.getDate() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getFullYear()
  );
}

//----------------------------------------------------------------------------------------------------------

//Ejercicio 4 (punto 5 del examen)

function loteria() {
  var num_premio = "";
  for (var i = 0; i < 5; i++) {
    //vamos sacando cada dígito del número de la loteria con un for
    var cifra = Math.floor(Math.random() * 10).toString(); //el valor convertido en string se almacena en la variable cifra
    num_premio = num_premio.concat(cifra); //num_premio va guardando cada cifra generada en el for.
  }
  alert("El número premiado es: " + num_premio); //muestra en un alert el número premiado de la lotería de navidad.
}
